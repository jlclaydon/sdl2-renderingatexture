/*
Begning SDL - Simple SDL example

*/

//For exit()
#include <stdlib.h>

//Include SDL library
#include "SDL.h"
#include "SDL_image.h"


const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture;
    SDL_Texture*     playerTexture;

    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    SDL_Init(SDL_INIT_EVERYTHING);

    gameWindow = SDL_CreateWindow("Hello my name is hooliganJosh",          // Window title
                              SDL_WINDOWPOS_UNDEFINED,  // X position
                              SDL_WINDOWPOS_UNDEFINED,  // Y position
                              WINDOW_WIDTH, WINDOW_HEIGHT,                 // width, height
                              SDL_WINDOW_SHOWN);        // Window flags

    // if the window creation succeeded create our renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    //Setup sprite
    temp = IMG_Load("assets/images/background.png");

    // NOTE: SDL is limited to loading BMPsNOTE: SDL is limited to loading BMPs
    //       We'll solve this problem later.

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);

    temp = nullptr;

    temp = IMG_Load("assets/images/dorf2.png");

    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    SDL_FreeSurface(temp);

    temp = nullptr;

    //Draw stuff here.
    SDL_Rect sourceRectangle;
    SDL_Rect targetRectangle;

    sourceRectangle.x = 0;
    sourceRectangle.y = 0;

    SDL_QueryTexture(playerTexture, 0, 0,
        &(sourceRectangle.w), //width
        &(sourceRectangle.h)); //height

    targetRectangle.x = 100;
    targetRectangle.y = 400;

    targetRectangle.w = sourceRectangle.w * 1.0f;
    targetRectangle.h = sourceRectangle.h * 1.0f;

    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

    SDL_RenderClear(gameRenderer);

    // 2. Draw the imageSDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);
    SDL_RenderCopy(gameRenderer, playerTexture, &sourceRectangle, &targetRectangle);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay( 5000 );

    //Clean up!
    SDL_DestroyTexture(backgroundTexture);
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);

    //Shutdown SDL - clear up resources etc. 
    SDL_Quit();

    exit(0);
}
